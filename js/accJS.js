var accOn = false,
popupOpen = true,
tabindex= 0,
tabindex01 = 0,
currentSection = "overview";

function setupAccessibility(){
    $(document).unbind('keypress');
    $(document).unbind('keydown');

    $(document).keyup(function (event) {
        var key = event.which;
		
		 if (key === 9) {
            $(document.activeElement).blur();
        }

        if (key === 65 && $("#first").is(":visible") == true) {
			
            $('#accDiv').toggle();
            $('#fiveEight').toggleClass('accessOn accessOff');
            $('#infoBtn').toggleClass('accessOn accessOff');
            $('#tutBtn').toggleClass('accessOn accessOff');
            $('#saveBtn').toggleClass('accessOn accessOff');
            $('#formulaBtn').toggleClass('accessOn accessOff');
            $('#videoBtn').toggleClass('accessOn accessOff');
           // $('.sideLetter').toggleClass('sideLetterAccessOn sideLetterAccessOff');
            $('#arrw_L').toggleClass('accessOffArrL');
            $('#arrw_R').toggleClass('accessOffArrR');
			$('#accessText').fadeToggle(400);
			$('.accTxt').toggleClass('accTxtOff');
            $('#tutNextBtn').toggleClass('accessNextBtnOn');
            $('#tutPrevBtn').toggleClass('accessPrevBtnOn');
            $('#first .exitBtn').trigger("click");

            accOn = true;
            $('#fiveEight').trigger("click");
            $('.acc').show();
            $('.accContentOn').show();
            $('.accContentOff').hide();
        }
			 /*if (showingParts) {
                $('.partsName').trigger("mouseout");
                currentPartNum = 0;
            }*/

        //IF ACCESSIBILITY IS TURNED ON
        if (accOn === true) { $(".contentBox").css('width','470');
								 $(".overviewBtn").css('min-width','125px');
								 $(".externalBtn").css('min-width','125px');
								 $(".internalBtn").css('min-width','125px');

            //HEADER BUTTONS NAVIGATION
            switch (key) {
                 case 49: //1
                 case 97:
                    if (popupOpen) {
                        if ($(currPopup + " .link1") !== null) {
                            $(currPopup + " .link1").trigger("click");
                        }
                    } else {
						if($('.titleBar').text() == 'OVERVIEW QUIZ' || $('.titleBar').text() == 'EXTERNAL QUIZ' || $('.titleBar').text() == 'INTERNAL QUIZ'){
                       // console.log("current section: " + currentSection);
                        $("#" + currentSection + "Quiz .answer:eq(0)").trigger("click");
						}
					}
                    break;
                case 50: //2
                case 98:
                    if (popupOpen) {
                        if ($(currPopup + " .link2") !== null) {
                            $(currPopup + " .link2").trigger("click");
                        }
                    } else {
						if($('.titleBar').text() == 'OVERVIEW QUIZ' || $('.titleBar').text() == 'EXTERNAL QUIZ' || $('.titleBar').text() == 'INTERNAL QUIZ'){
                       // console.log("current section: " + currentSection);
                        $("#" + currentSection + "Quiz .answer:eq(1)").trigger("click");
						}
					}
					
                    break;
                case 51: //3
                case 99:
                    if (popupOpen) {
                        if ($(currPopup + " .link3") !== null) {
                            $(currPopup + " .link3").trigger("click");
                        }
                    } else {
						if($('.titleBar').text() == 'OVERVIEW QUIZ' || $('.titleBar').text() == 'EXTERNAL QUIZ' || $('.titleBar').text() == 'INTERNAL QUIZ'){
                       // console.log("current section: " + currentSection);
                        $("#" + currentSection + "Quiz .answer:eq(2)").trigger("click");
						}
				    }
                    
                    break;
                case 52: //4
                case 100:
                    if (popupOpen) {
                        if ($(currPopup + " .link4") !== null) {
                            $(currPopup + " .link4").trigger("click");
                        }
                    } else {
						if($('.titleBar').text() == 'OVERVIEW QUIZ' || $('.titleBar').text() == 'EXTERNAL QUIZ' || $('.titleBar').text() == 'INTERNAL QUIZ'){
                       // console.log("current section: " + currentSection);
                        $("#" + currentSection + "Quiz .answer:eq(3)").trigger("click");
						}
					}					
                    break;
                case 53: //5
                case 101:
                    if (popupOpen) {
                        if ($(currPopup + " .link5") !== null) {
                            $(currPopup + " .link5").trigger("click");
                        }
                    } else {

                    }
                    break;
               
               case 84:   //T
                    if (popupOpen) {
                    } else {
                        $("#tutBtn").trigger("click");
                    }
                    break;
              
                case 32:     // SPACE BAR
                   if (popupOpen) {
                        $(currPopup + " .exitBtn").trigger("click");
                    }
					
                    break;
            }
            switch (key) {
                
              
                case 73:  //I
                    if($('#acc_internal').is(':visible') && !popupOpen )
					{                 
                        $('#internalBtn').trigger("click");
						$('#acc_overview01').hide();
						$('#acc_external').hide();
						$('#acc_internal').hide();
						
                        
                        currentSection = "internal";
                    }
                    break;
                case 86:  //V
                    if (popupOpen) {
                        
                    } else {
                        
                    }
                    break;
                case 70:  //F
                    if (popupOpen) {
                        
                    } else {
                        
                    }
                    break;
               
            }
            switch (key) {
                case 80:  //P
				 	if (!popupOpen && !$('.dataSlide').hasClass('svgSlide'))  {
                   		$('#' + currentSection + 'Holder .dataSlide .procBtn, .checkBtn').trigger("click");
						
				 	}
					if($('.partsDetail ').hasClass('dataSlide')){
				        $('.partsDetail.dataSlide .popBtn').trigger("click");
					}
					
					break;
					
				case 82:  //R
				 	
					
                case 37:  //LEFT ARROW
                    if (popupOpen) {
                        if ($(currPopup + " .arrowL_P") !== null) {
                            $(currPopup + " .arrowL_P").trigger("click");
                        }

                    } else {
                        $('#arrw_L').trigger("click");               
                    }
                    break;
                case 39:  //RIGHT ARROW
                    if (popupOpen) {
                        if ($(currPopup + " .arrowR_P") !== null) {
                            $(currPopup + " .arrowR_P").trigger("click");
                        }
                    } else {
                        $('#arrw_R').trigger("click");                   
                    }
                    break;
                case 68:  //D
                    if (popupOpen) {
                        
                    } else {
                        
                    }
                    break;
                case 69:  //E
                    if($('#acc_external').is(':visible') && !popupOpen )
					{                 
                        $('#externalBtn').trigger("click");
						$('#acc_overview01').hide();
						$('#acc_internal').hide();
						$('#acc_external').hide();
                        currentSection = "external";
                    }
					
                    break;
				case 79: //O
					if($('#acc_overview01').is(':visible') && !popupOpen )
					{                 
                        $('#overviewBtn01').trigger("click");
						$('#overviewBtn').trigger("click");
						$('#acc_overview01').hide();
						$('#acc_external').hide();
						$('#acc_internal').hide();
                        
                        currentSection = "overview";
                    }
					
					break;
					
				case 188: //< - previous slide external/ internal
					if($('#external01').hasClass('dataSlide') && !popupOpen )
					{                 
                        $('.externalImgMain .prev').trigger("click");
						tabindex01= 0;
                    }
					if($('#internal01').hasClass('dataSlide') && !popupOpen )
					{                 
                        $('.internalImgMain .prev').trigger("click");
						tabindex= 0;
                    }
					
					
                    break;
				case 190: //> - next slide external /internal
					if($('#external01').hasClass('dataSlide') && !popupOpen )
					{                 
                        $('.externalImgMain .next').trigger("click");
						tabindex01= 0;
						
                    }
					if($('#internal01').hasClass('dataSlide') && !popupOpen )
					{                 
                        $('.internalImgMain .next').trigger("click");
						tabindex= 0;
						
                    }
                    break;
					
				case 219://previousPart:   //Previous part of external section
					
					if($('#internal01').hasClass('dataSlide') && !popupOpen ){
					
					if(tabindex>1){	 tabindex--; }else{tabindex = $('#internal01 .imgSlider .slideItem.active .hoverBlue').length;}		
					 $('#internal01 .hoverBlue').each(function() {
							if($(this).is('.partsData')){
								$(this).attr('class', 'hoverBlue partsData');
							} else {
								$(this).attr('class', 'hoverBlue');
							}	
						});
					  var element =  $('#internal01 .imgSlider .slideItem.active .hoverBlue[data-tabindex="'+(tabindex )+'"]'); 

					$("#internal01 .partsName").removeClass('visibleText');
					  element.trigger("mouseover");
					  if(element.is('.partsData')){
							element.attr('class', 'partsData hoverBlue hoveractive');	
						} else {
							element.attr('class', ' hoverBlue hoveractive');	
							}
					   	
					
				}
				
				
				if($('#external01').hasClass('dataSlide') && !popupOpen ){
					
					if(tabindex01>1){	tabindex01--;}else{tabindex01 = $('#external01 .imgSlider .slideItem.active .hoverBlue').length}
					$('#leg, .hoverActive').css('fill', '');		
					 $('#external01 .hoverBlue').each(function() {
							if($(this).is('.partsData')){
								$(this).attr('class', 'hoverBlue partsData');
							} else {
								$(this).attr('class', 'hoverBlue');
							}	
						});
					  var element =  $('#external01 .imgSlider .slideItem.active .hoverBlue[data-tabindex="'+(tabindex01)+'"]'); 

					$("#external01 .partsName").removeClass('visibleText');
					  element.trigger("mouseover");
					  if(element.is('.partsData')){
							element.attr('class', 'partsData hoverBlue hoveractive');	
						} else {
							element.attr('class', ' hoverBlue hoveractive');	
							}
					   	
					
				}
                	break;
            case 221://nextPart: //key code : 221 - Next Part External
				if($('#internal01').hasClass('dataSlide') && !popupOpen ){
					
					if(tabindex< $('#internal01 .imgSlider .slideItem.active .hoverBlue').length){ tabindex++;
					   }	else {tabindex=0; tabindex++}
					 
					 $('#internal01 .hoverBlue').each(function() {
						 //console.log($(this).class);
							if($(this).is('.partsData')){
								//alert('yes');
								$(this).attr('class', 'hoverBlue partsData');
							} else {
								//alert('no parts');
								$(this).attr('class', 'hoverBlue');
							}	
						});
					  var element =  $('#internal01 .imgSlider .slideItem.active .hoverBlue[data-tabindex="'+(tabindex)+'"]'); 
						//console.log(element);
					$("#internal01 .partsName").removeClass('visibleText');
					  element.trigger("mouseover");
					  if(element.is('.partsData')){
						  
							element.attr('class', 'partsData hoverBlue hoveractive');	
						} else {
							//alert('parts new');
							element.attr('class', ' hoverBlue hoveractive');	
							}
				}
				
				if($('#external01').hasClass('dataSlide') && !popupOpen ){
					
					if(tabindex01< $('#external01 .imgSlider .slideItem.active .hoverBlue').length){tabindex01++;}
					else{tabindex01=0; tabindex01++;}
					 $('#leg,.hoverActive').css('fill', '');
					 $('#external01 .hoverBlue').each(function() {
						 //console.log($(this).class);
							if($(this).is('.partsData')){
								//alert('yes');
								$(this).attr('class', 'hoverBlue partsData');
							} else {
								//alert('no parts');
								$(this).attr('class', 'hoverBlue');
							}	
						});
					  var element =  $('#external01 .imgSlider .slideItem.active .hoverBlue[data-tabindex="'+(tabindex01)+'"]'); 
						//console.log(element);
					$("#external01 .partsName").removeClass('visibleText');
					  element.trigger("mouseover");
					  if(element.is('.partsData')){
						  
							element.attr('class', 'partsData hoverBlue hoveractive');	
						} else {
							//alert('parts new');
							element.attr('class', ' hoverBlue hoveractive');	
							}
					   	
					
				}
				
			   
			   
                break;
			case 13: // Enter- Select feature 
			
				if($('.dataSlide').hasClass('dataSlide') && !popupOpen ){
					
					$( ".hoveractive.partsData").trigger("click");					
				}
					
                  
            }   
        }
    });
}