$('#contentArea').load('content.html', function () {
    'use strict';
    isLoaded('contentScript');
});

function setupContent() {
    'use strict';
	contentDocumentReady();
	getTitle();
	overviewQuestion();
	externalQuestion();
	internalQuestion();
	//setupMainMin();
    
}
function contentDocumentReady() { 

/* hover to show parts name */
$(".hoverBlue").mouseout(function() { $(".partsName").removeClass('visibleText'); });
$("#antennae").mouseover(function() {$(".antennae_text").addClass('visibleText'); });
$("#walking-legs").mouseover(function() {$(".walking_lags_text").addClass('visibleText');$('#leg').css('fill', '#a6d3ea');});
$("#walking-legs").mouseout(function() { $('#leg').css('fill', '');	$(".partsName").removeClass('visibleText'); });
$("#first_leg").mouseover(function() {$(".cheliped_text").addClass('visibleText');}); 
$("#uropods02").mouseover(function() {$(".uropods02_text").addClass('visibleText'); }); 
$("#telson").mouseover(function() {$(".telson_top_text").addClass('visibleText'); }); 
$("#anus02").mouseover(function() {$(".anus02_text").addClass('visibleText'); }); 
$("#swimmerets02").mouseover(function() {$(".swimmerets02_text").addClass('visibleText'); }); 
$("#antennae02").mouseover(function() {$(".antennace02_text").addClass('visibleText'); }); 
$("#mouth-parts02").mouseover(function() {$(".mouth_parts02_text").addClass('visibleText'); }); 
$("#Cheliped02").mouseover(function() {$(".Cheliped02_text").addClass('visibleText'); }); 
//$(".walking-legs-bottom02").mouseover(function() {$(".walking_legs02_text").addClass('visibleText'); }); 
$("#walking-legs-bottom02").mouseover(function() {$(".walking_legs02_text").addClass('visibleText');$('#Cheliped02 .hoverActive').css('fill', '#a6d3ea');});
$("#walking-legs-bottom02").mouseout(function() { $('#Cheliped02 .hoverActive').css('fill', '');$(".partsName").removeClass('visibleText'); });
 $("#cheliped").mouseover(function() {$(".cheliped_text").addClass('visibleText'); });
$("#abdomen").mouseover(function() {$(".abdomen_text").addClass('visibleText'); });
$("#cephalothorax").mouseover(function() {$(".cephalothorax_text").addClass('visibleText'); });


/* top part */
$("#uropods").mouseover(function() {$(".uropods_top_text").addClass('visibleText'); });
$("#abdomen-top").mouseover(function() {$(".abdomen_top_text").addClass('visibleText'); });
$("#cheliped-leg-top").mouseover(function() {$(".cheliped_top_text").addClass('visibleText'); });
$("#compound-eyes-top").mouseover(function() {$(".compound_eyes_top_text").addClass('visibleText'); });
$("#antennae_top").mouseover(function() {$(".antennae_top_text").addClass('visibleText'); });
$("#cephalothorax_top").mouseover(function() {$(".cephalothorax_top_text").addClass('visibleText'); });

$("#walking-legs-top").mouseover(function() {
	$(".walking_legs_top_text").addClass('visibleText');
	$('#cheliped-leg-top .hoverActive').css('fill', '#a6d3ea');
 });
$("#walking-legs-top").mouseout(function() { 
	$('#cheliped-leg-top .hoverActive').css('fill', '');
	$(".partsName").removeClass('visibleText'); 
	
});

/* internal part hover */
$("#heart").mouseover(function(){$(".heart_top_text").addClass('visibleText'); }); 
$("#liver").mouseover(function(){$(".liver_top_text").addClass('visibleText'); }); 
$("#stomach").mouseover(function(){$(".stomach_top_text").addClass('visibleText'); }); 
$("#abdomen_muscles").mouseover(function(){$(".abdomen_muscles_side_text").addClass('visibleText'); }); 
$("#gills").mouseover(function(){$(".gills_side_text").addClass('visibleText'); }); 
$("#green_bland").mouseover(function(){$(".green_gland_side_text").addClass('visibleText'); }); 



	/* index toggle*/
	$('.indexTitle').click(function() {
		$(this).toggleClass('indexActive');
		$(this).siblings('.indexBody').slideToggle('slow');
	});
	/* index sub nav*/
	$('.indexFirstLav').click(function() {
                if ($(this).hasClass('indeSubActive')) {
                    $(this).next().slideUp();
                    $(this).removeClass('indeSubActive');
                    $(this).next('.subNav').removeClass('openBox');
                } else {
                    $('.indexFirstLav').removeClass('indeSubActive');
                    $(this).addClass('indeSubActive');
                    $('.subNav').removeClass('openBox');
                    $(this).next('.subNav').addClass('openBox');
                    $('.subNav').slideUp();
                    $(this).next().slideDown();
                }
            }).filter(':first').click();

/* Data Click*/

$(document).on("click", ".procBtn, .externalBtn, .internalBtn, .overviewBtn", function() {
	var tabId = $(this).attr('data-next');
	$(".data_parent").removeClass("dataSlide");
    var pageTitle = $("#" + tabId).parents('.page').attr('data-title');
    $('.titleBar').text(pageTitle);
	setTimeout(function() {
	 $("#" + tabId).addClass("dataSlide");
	 }, 300);
});

/* For external parts popups*/
$(document).on("click", "#external .partsData", function(){
	$('.externalTitle').slideUp(300);
	var tabId = $(this).attr('data-detail');
	$('#external .partsData').attr('class', 'hoverBlue');
	$("#external01 .partsName").removeClass('visibleText');
	var string = $(this).attr('data-nextpart');
	var arr = string.split(',');
	$.each(arr,function(v,k){
		$('#' + k).attr('class', 'partsData hoverBlue');
	});
	var nextPartName = $(this).attr('data-partext');
	setTimeout(function() {$('.externalTitle').html(nextPartName)},400);
	$("#external .partsDetail").removeClass("dataSlide");
		//setTimeout(function(){
		$("#" + tabId).addClass("dataSlide");
	 //}, 400);
	 
	
	
});



/* For internal parts popups*/
$(document).on("click", "#internal .partsData", function(){
	$('.internalTitle').slideUp(300);
	var tabId = $(this).attr('data-detail');
	$('#internal .partsData').attr('class', 'hoverBlue');
	$("#internal01 .partsName").removeClass('visibleText');
	var string = $(this).attr('data-nextpart');
	var arr = string.split(',');
	$.each(arr,function(v,k){
		$('#' + k).attr('class', 'partsData hoverBlue');
	});
	var nextPartName = $(this).attr('data-partext');
	//console.log(nextPartName);
	setTimeout(function() {$('#internal .internalTitle').html(nextPartName) },400);
	//if($(".internalTitle").css('display','none')){alert("done")}
	$("#internal .partsDetail").removeClass("dataSlide");
	//var pageTitle = $("#" + tabId).parents('.page').attr('data-title');
	//$('.titleBar').text(pageTitle);
		//setTimeout(function(){
			$("#" + tabId).addClass("dataSlide");
	// }, 400);
});

$(document).on('click','.closepopup', function() {
		$(this).parents(".partsDetail").removeClass("dataSlide");
		$('.internalTitle').slideDown(300);
		$('.externalTitle').slideDown(300);
	
	});
		
/* img slider */			
 var currentIndex = 0,
  items = $('.externalImgMain .imgSlider .slideItem'),
  itemAmt = items.length;

function cycleItems() {
  var item = $('.externalImgMain .imgSlider .slideItem').eq(currentIndex);
  items.hide();
 $('.externalImgMain .imgSlider .slideItem').removeClass('active');
  item.css('display','inline-block').addClass('active');
  var pageTitle = item.attr('data-title')
  $('.titleBar').text(pageTitle);
}
		
		
		var overviewBtn = $('.overviewBtn');  //overview Button
		var internalBtn = $('.internalBtn');
		var externalBtn = $('.externalBtn');
		var procBtn = $('#overviewBtn'); //proceed button
		
		
		procBtn.on("click",function(e) {
			
			internalBtn.prop("disabled",true);
			$(".internalBtn").addClass("indeactive");
			externalBtn.prop("disabled",true);
			$(".externalBtn").addClass("exdeactive");	
		});
		overviewBtn.on("click",function(e) {
			
			//$('#secPage').html("<div class='slideSec'><div class='SlideTitleIn'>That's It!</div><div class='slideCon'><p>You have viewed and read about the Crayfish Anatomy information available in this module.</p><p>If you believe that you are familiar with all of the concepts presented here, proceed to the next step in this course.</p><p>Choose either of the two button below.</p></div>");
			
			
			internalBtn.prop("disabled",true);
			$(".internalBtn").addClass("indeactive");
			externalBtn.prop("disabled",true);
			$(".externalBtn").addClass("exdeactive");	
			overviewBtn.css('pointer-events','none'); // disable overview button after first click // re-enable in quiz
			
		});
		externalBtn.on("click",function(e) {
			
			internalBtn.prop("disabled",true);
			$(".internalBtn").addClass("indeactive");
			overviewBtn.prop("disabled",true);
			$(".overviewBtn").addClass("ovdeactive");	
			externalBtn.css('pointer-events','none');
		});
		internalBtn.on("click",function(e) {
			
			externalBtn.prop("disabled",true);
			$(".externalBtn").addClass("exdeactive");
			overviewBtn.prop("disabled",true);
			$(".overviewBtn").addClass("ovdeactive");	
			internalBtn.css('pointer-events','none');
		});
		procBtn.on("click",function(e){
						$('#acc_overview01').hide();
						$('#acc_external').hide();
						$('#acc_internal').hide();
					});

$(document).on('click','.externalImgMain .next' ,function() {
  //clearInterval(autoSlide);
  
  currentIndex += 1;
  if (currentIndex > itemAmt - 1) {
    currentIndex = 0;
  }
  cycleItems();
});

$(document).on('click','.externalImgMain .prev',function() {
  //clearInterval(autoSlide);
  currentIndex -= 1;
  if (currentIndex < 0) {
    currentIndex = itemAmt - 1;
  }
  cycleItems();
});



/* internal slider */
/* img slider */			
 var currentIndex_new = 0,
  items_new = $('.internalImgMain .imgSlider .slideItem'),
  itemAmt_new = items_new.length;

function cycleItems_new() {
  var item_new = $('.internalImgMain .imgSlider .slideItem').eq(currentIndex_new);
  items_new.hide();
  $('.internalImgMain .imgSlider .slideItem').removeClass('active');
  item_new.css('display','inline-block').addClass('active');
  var pageTitle = item_new.attr('data-title')
  $('.titleBar').text(pageTitle);
  
}



$(document).on('click','#internal01 .next',function() {
  //clearInterval(autoSlide);
  currentIndex_new += 1;
  if (currentIndex_new > itemAmt_new - 1) {
    currentIndex_new = 0;
  }
  cycleItems_new();
});

$('#internal01 .prev').click(function() {
  //clearInterval(autoSlide);
  currentIndex_new -= 1;
  if (currentIndex_new < 0) {
    currentIndex_new = itemAmt_new - 1;
  }
  cycleItems_new();
});


	

	
}
function getTitle() {
	$('.overview').click(function() {
    var x = document.getElementsByClassName("titleBar");
    x[0].innerHTML = "OVERVIEW";
	});
	
	$('.procBtnOQuiz').click(function() {
    var x = document.getElementsByClassName("titleBar");
    x[0].innerHTML = "OVERVIEW QUIZ";
	});
	
	$('.procBtnEQuiz').click(function() {
	$('.externalTitle').slideDown();
    var x = document.getElementsByClassName("titleBar");
    x[0].innerHTML = "EXTERNAL QUIZ";
	});
	$('.procBtnIQuiz').click(function() {
	$('.internalTitle').slideDown();	
    var x = document.getElementsByClassName("titleBar");
    x[0].innerHTML = "INTERNAL QUIZ";
	});
	$('.internalBtn').click(function() {
    var x = document.getElementsByClassName("titleBar");
    x[0].innerHTML = "INTERNAL SECTION";
	});
	
	$('.externalBtn').click(function() {
    var x = document.getElementsByClassName("titleBar");
    x[0].innerHTML = "EXTERNAL SECTION";
	});
	
	
	
}


