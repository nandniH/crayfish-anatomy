$('#sideMenu #loadDiv').load("sidePanel_noAccordion.html #sidePanelContent", function () {
    
    $('#sideMenu #arrowHolder #arrw_L').click(function(){
        changeInstructions('left');
    });
    
    $('#sideMenu #arrowHolder #arrw_R').click(function(){
        changeInstructions('right');
    });
    
    $('#sideMenu #procedurePanel').children('.inst').hide();
    $('#sideMenu #procedurePanel').children('.inst:first').show();
    
    updateInstructionsProgress();
    
    setupDolts();
    
    isLoaded('sidePanelsScript');
});

function updateInstructionsProgress() {
    var currentInstructionNum = $('#sideMenu #procedurePanel .inst').index($('#sideMenu #procedurePanel .inst:visible'));
    
    $('#doltContainer ul li').removeClass('active');
    $('#doltContainer ul li').eq(currentInstructionNum).addClass('active');
}

function changeInstructions(direction) {
    var newInstruction;
    
    switch (direction) {
        case 'left':
            newInstruction = $('#sideMenu #procedurePanel .inst:visible').prev('#sideMenu #procedurePanel .inst');
            
            if(newInstruction.hasClass('inst')){
                newInstruction.show();
                $('#sideMenu #procedurePanel').children('.inst:visible:last').hide();   
            }
            
            break;
        case 'right':
            newInstruction = $('#sideMenu #procedurePanel .inst:visible').next('#sideMenu #procedurePanel .inst');
            
            if(newInstruction.hasClass('inst')){
                newInstruction.show();
                $('#sideMenu #procedurePanel').children('.inst:visible:first').hide();
            }
            break;
    }
    
    updateInstructionsProgress();
}


/*==================================
 *  DOLT CONTAINER
 =================================*/

function setupDolts() {
    var numInstructions = $('#sideMenu #procedurePanel').children('.inst').length,
        i;
    $('#doltContainer ul').empty();
    if (numInstructions > 1) {
        for (i = 0; i < numInstructions; i++) {
            $('#doltContainer ul').prepend('<li class="slideNum inactive" id="slidenum' + i + '"></li>');
        }
    }
    updateInstructionsProgress();
}