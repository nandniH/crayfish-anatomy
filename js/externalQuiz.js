function externalQuestion(){
		
var questionNum=0;
var questionBanks=new Array();
var currentQ="#currentQ";
var nextQ=new Object;
var queLock=false;
var numOfQuestions;
var exscore=0;
		 

 		questionBanks=new Array;
			questionBanks=[["How do the walking legs help a crayfish?","Locomotion and oxygenation","Leaping tall building","Thinking and sight","Handling and eating food"],
			["What section of the crayfish assists in handling and eating food?","Mouthparts","Abdomen","Walking legs","Tail"],
			["What shields the dorsal surface of the cephalothorax?","Carapace", "Polyurethane","Cheliped","Titanium"],
			["What is one of the functions of the antennae?","Touch","Protection","Handling food","Locomotion"],
			["Which parts form the fan-like tail of the crayfish?","Telson and uropods","Mandible and maxillae","Feathers and ligaments","Chelipeds and swimmerets"],
			["The cephalothorax is the fusion of which regions?","Cephalic (head) and thoracic","Telson and uropods","Chelipeds and walking legs","Abdomen and tail"],
			["Where does indigestible matter exit the body of the crayfish?","Anus","Heart","Green gland","Gills"],
			["Which crayfish walking leg also provides defense and offense?","Cheliped","Telson","Antennae","Maxilliped"],
			["What type of eye contains thousands of tiny prismatic lenses?","Compound eyes","Pit eyes","Reflector eyes","None, they don’t exist"],
			["Where are the crayfish swimmerets located?","Abdomen","Cephalothorax","Chelipeds","Telson"],
			["How many segments is the crayfish abdomen divided into?","6","8","12","3"]]

		
		 numOfQuestions=(questionBanks.length-6); 
		
		 scrambleDatabaseEx();
		 displayQuestionEx();

 function scrambleDatabaseEx(){
	for(i=0;i<50;i++){ 
	var rnd1=Math.floor(Math.random()*questionBanks.length);
	var rnd2=Math.floor(Math.random()*questionBanks.length);
	 
	var temp=questionBanks[rnd1];
	questionBanks[rnd1]=questionBanks[rnd2];
	questionBanks[rnd2]=temp;
	
	}//i
	 
 }//scdb
 
function displayQuestionEx(){
var x=5;
var y=8;
var rnds=Math.floor(Math.random()*((y-x)+1)+x);

 var q1;
 var q2;
 var q3;
 var q4;

if(rnds==5){q1=questionBanks[questionNum][1];q2=questionBanks[questionNum][2];q3=questionBanks[questionNum][3];q4=questionBanks[questionNum][4];}
if(rnds==6){q2=questionBanks[questionNum][1];q3=questionBanks[questionNum][2];q4=questionBanks[questionNum][3];q1=questionBanks[questionNum][4];}
if(rnds==7){q3=questionBanks[questionNum][1];q4=questionBanks[questionNum][2];q1=questionBanks[questionNum][3];q2=questionBanks[questionNum][4];}
if(rnds==8){q4=questionBanks[questionNum][1];q3=questionBanks[questionNum][2];q2=questionBanks[questionNum][3];q1=questionBanks[questionNum][4];}

$(currentQ).append('<div class="SlideQuestion">'+questionBanks[questionNum][0]+'</div><div id="5" class="customRadios answer">'+q1+'</div><div id="acc_5" class="accTxt"><div class="arrow-left"></div><p>1</p></div><div id="6" class="customRadios answer">'+q2+'</div><div id="acc_6" class="accTxt"><div class="arrow-left"></div><p>2</p></div><div id="7" class="customRadios answer">'+q3+'</div><div id="acc_7" class="accTxt"><div class="arrow-left"></div><p>3</p></div><div id="8" class="customRadios answer">'+q4+'</div><div id="acc_8" class="accTxt"><div class="arrow-left"></div><p>4</p></div>');
if(accOn === true){$('#acc_5, #acc_6, #acc_7, #acc_8').css('visibility','visible'); }

 $('.customRadios').click(function(){
	
  if(queLock==false){queLock=true;	
   $('#5').css("opacity",0.5);
	 $('#6').css("opacity",0.5);
	 $('#7').css("opacity",0.5);
	$('#8').css("opacity",0.5);	 
		
	$('#'+this.id).css("opacity",1);	
	$('#'+rnds).css("opacity",1);
  $('.customRadios').addClass('no-hover');
  //correct answer
  if(this.id==rnds){
	    $(this).css( "background", "rgba(0, 160, 81, 0.23)","color", "#000");
		//$(this).css('opacity',1);
   //$(stage).append('<div class="feedback1">CORRECT</div>');
   exscore++;
   }
  //wrong answer	
  if(this.id!=rnds){
  // $(stage).append('<div class="feedback2">WRONG</div>');
  	 $(this).css( "background", "rgba(231, 28, 36, 0.25)","color", "#000");
     $('#' + rnds).css( "background", "rgba(0, 160, 81, 0.23)","color", "#000");
	// $('#' + rnds).css('opacity',1);
  }
  
  $(currentQ).append('<a class="checkBtn"  href="#">Proceed </a><div id="acc_exquiz" class="accTxt"><div class="arrow-right"></div><p>P</p></div>');
   if(accOn === true){$('#acc_exquiz').css('visibility','visible'); }
   $(".checkBtn").click(function() { 
   changeQuestionEx() ;
  });
  //setTimeout(function(){changeQuestion()},2000);
 }})
}//display question


	function changeQuestionEx(){
		
		questionNum++;
	
	if(currentQ=="#currentQ"){
		nextQ="#currentQ";
		currentQ="#nextQ";
	}
		else{
			nextQ="#nextQ";
			currentQ="#currentQ";
			}
	
	if(questionNum<numOfQuestions){
		displayQuestionEx();
		}
		else{
			displayFinalSlideEx();
			}
	
	 $(nextQ).animate({},"slow", function() {$(nextQ),$(nextQ).empty();});
	 $(currentQ).animate({},"slow", function() {queLock=false;});
	}//change question
	
	
	function displayFinalSlideEx(){
		
		$(currentQ).append('<div class="SlideQuestion">You have answered  '+exscore+' correct out of '+numOfQuestions+' questions.</div>');
		$(currentQ).append("<p class='slideCon'>Now select either the Overview or Internal button below.</p>");
	 	//$(currentQ).append('<br><div class="retryex">Retry</div>');
		$('.externalBtn').click(function(){exscore=0;questionNum=-1;changeQuestionEx();});		
			var overviewBtn = $('.overviewBtn');
			var internalBtn = $('.internalBtn');
			var externalBtn = $('.externalBtn');
	 		internalBtn.prop("disabled",false);
			$(".internalBtn").removeClass("indeactive");
			overviewBtn.prop("disabled",false);
			$(".overviewBtn").removeClass("ovdeactive");
			internalBtn.css('pointer-events','');
			overviewBtn.css('pointer-events','');
			
			externalBtn.prop("disabled",true);
			$(".externalBtn").addClass("exdeactive");
			
			$('#acc_overview01').show();
			$('#acc_external').hide();
			$('#acc_internal').show();
	}//display final slide

	
	}//doc ready
	