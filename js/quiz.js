function overviewQuestion(){
		
var questionNumber=0;
var questionBank=new Array();
var currentQuestion="#currentQue";
var nextQuestion=new Object;
var questionLock=false;
var numberOfQuestions;
var score=0;
		 

 		questionBank=new Array;
			questionBank=[["Why is a crayfish considered a coelomate?","Possesses internal body cavities","Lives in freshwater","Body cavities are embedded in the mesoderm","Friendly with other species"],
			["What type of symmetry do arthropods exhibit?","Bilateral","Radial","Biradial","Spherical"],
			["What is the main substance found in the crayfish's exoskeleton?","Chitin", "Collagen","Keratin","Cellulose"],
			["What process does a crayfish use to grow and regenerate?","Molting","Symbiosis","Photosynthesis","Metamorphosis"],
			["What covers the crayfish and gives it protection?","Exoskeleton","Hydrostatic skeleton","Endoskeleton","Cytoskeleton"],
			["What phylum does the crayfish belong to?","Arthropoda","Chordata","Loricifera","Brachiopoda"]]

		
		 numberOfQuestions=questionBank.length-1; 
		
		scrambleDatabase();
		displayQuestion();

  function scrambleDatabase(){
	for(i=0;i<50;i++){ 
	var rnd1=Math.floor(Math.random()*questionBank.length);
	var rnd2=Math.floor(Math.random()*questionBank.length);
	 
	var temp=questionBank[rnd1];
	questionBank[rnd1]=questionBank[rnd2];
	questionBank[rnd2]=temp;
	
	}//i
	 
 }//scdb



function displayQuestion(){
var rnd=Math.random()*4;
rnd=Math.ceil(rnd);
 var q1;
 var q2;
 var q3;
 var q4;

if(rnd==1){q1=questionBank[questionNumber][1];q2=questionBank[questionNumber][2];q3=questionBank[questionNumber][3];q4=questionBank[questionNumber][4];}
if(rnd==2){q2=questionBank[questionNumber][1];q3=questionBank[questionNumber][2];q4=questionBank[questionNumber][3];q1=questionBank[questionNumber][4];}
if(rnd==3){q3=questionBank[questionNumber][1];q4=questionBank[questionNumber][2];q1=questionBank[questionNumber][3];q2=questionBank[questionNumber][4];}
if(rnd==4){q4=questionBank[questionNumber][1];q3=questionBank[questionNumber][2];q2=questionBank[questionNumber][3];q1=questionBank[questionNumber][4];}

$(currentQuestion).append('<div class="SlideQuestion">'+questionBank[questionNumber][0]+'</div><div id="1" class="customRadio answer" >'+q1+'</div><div id="acc_1" class="accTxt"><div class="arrow-left"></div><p>1</p></div><div id="2" class="customRadio answer">'+q2+'</div><div id="acc_2" class="accTxt"><div class="arrow-left"></div><p>2</p></div><div id="3" class="customRadio answer">'+q3+'</div><div id="acc_3" class="accTxt"><div class="arrow-left"></div><p>3</p></div><div id="4" class="customRadio answer">'+q4+'</div><div id="acc_4" class="accTxt"><div class="arrow-left"></div><p>4</p></div>');
if(accOn === true){$('#acc_1, #acc_2, #acc_3, #acc_4').css('visibility','visible'); }

 $('.customRadio').click(function(){
	
  if(questionLock==false){questionLock=true;
   $('#1').css("opacity",0.5);
	 $('#2').css("opacity",0.5);
	 $('#3').css("opacity",0.5);
	$('#4').css("opacity",0.5);	 
		
	$('#'+this.id).css("opacity",1);	
	$('#'+rnd).css("opacity",1);
  $('.customRadio').addClass('no-hover');	
  //correct answer
  if(this.id==rnd){
	    $(this).css( "background", "rgba(0, 160, 81, 0.23)","color", "#000");
  
   score++;
   }
  //wrong answer	
  if(this.id!=rnd){
 
  	 $(this).css( "background", "rgba(231, 28, 36, 0.25)","color", "#000");
    $('#' + rnd).css( "background", "rgba(0, 160, 81, 0.23)","color", "#000");
	
  }
  
  $(currentQuestion).append('<a class="checkBtn"  href="#">Proceed </a><div id="acc_quiz" class="accTxt"><div class="arrow-right"></div><p>P</p></div>');
   if(accOn === true){$('#acc_quiz').css('visibility','visible'); }
   $(".checkBtn").click(function() { 
   changeQuestion() ;
        
  });
  
 }})
}//display question

	function changeQuestion(){
		
		questionNumber++;
	
	if(currentQuestion=="#currentQue"){
		nextQuestion="#currentQue";
		currentQuestion="#nextQue";
	}
		else{
			nextQuestion="#nextQue";
			currentQuestion="#currentQue";
			}
	
	if(questionNumber<numberOfQuestions){
		displayQuestion();
		}
		else{
			displayFinalSlide();
			}
	
	 $(nextQuestion).animate({},"slow", function() {$(nextQuestion),$(nextQuestion).empty();});
	 $(currentQuestion).animate({},"slow", function() {questionLock=false;});
	}//change question
	

	
	
	function displayFinalSlide(){
		
		$(currentQuestion).append('<div class="SlideQuestion">You have answered  '+score+' correct out of '+numberOfQuestions+' questions.</div>');
		$(currentQuestion).append("<p class='slideCon'>Now select either the External or Internal button below.</p>");
		//$(currentQuestion).append('<a href="#" class="retry">Retry Quiz</a><div id="acc_retry" class="accTxt"><div class="arrow-right"></div><p>R</p></div>');
		//if(accOn === true){$('#acc_retry').css('visibility','visible'); }
		$('.overviewBtn').click(function(){score=0;questionNumber=-1;changeQuestion();});	
			
			var overviewBtn = $('.overviewBtn');
			var internalBtn = $('.internalBtn');
			var externalBtn = $('.externalBtn');
	 		internalBtn.prop("disabled",false);
			$(".internalBtn").removeClass("indeactive");
			externalBtn.prop("disabled",false);
			$(".externalBtn").removeClass("exdeactive");
			externalBtn.css('pointer-events','');
			internalBtn.css('pointer-events','');
			
			overviewBtn.prop("disabled",true);
			$(".overviewBtn").addClass("ovdeactive");
			$('#acc_overview01').hide();
			$('#acc_external').show();
			$('#acc_internal').show();
			
	}//display final slide
	
	
	
	
	return false;
	
	}//doc ready