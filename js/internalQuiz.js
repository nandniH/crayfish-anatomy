function internalQuestion(){
		
var questionNo=0;
var queBanks=new Array();
var curQue="#curQue";
var nexQue=new Object;
var queLocks=false;
var numOfQue;
var inscore=0;
		 

 		queBanks=new Array;
			queBanks=[["What is another name for the cardiac chamber of the crayfish stomach?","Gastric mill","Green gland","Liver","Heart"],
			["How does a crayfish obtain oxygen?","Gills","Liver","Crayfish don't need oxygen","Lungs"],
			["Which organ allows nutrients to be absorbed into crayfish blood?","Digestive gland or liver", "Stomach","Green gland or kidney","Heart"],
			["Which muscle groups in the abdomen help a crayfish propel backward?","Flexor and extensor","Bicep and tricep","Deltoid and trapezius","Pectoral and coracoid"],
			["What is the function of the crayfish heart?","Pumps blood","Digests food","Processes Information","Aids in reproduction"],
			["Which organ excretes nitrogen waste?","Green gland or kidney","Digestive gland or liver","Heart","Gills"]]

		
		 numOfQue=(queBanks.length-1); 
		 scrambleDatabaseIn();
		 displayQuestionIn();
		
function scrambleDatabaseIn(){
	for(i=0;i<50;i++){ 
	var rnd1=Math.floor(Math.random()*queBanks.length);
	var rnd2=Math.floor(Math.random()*queBanks.length);
	 
	var temp=queBanks[rnd1];
	queBanks[rnd1]=queBanks[rnd2];
	queBanks[rnd2]=temp;
	
	}//i
	 
 }//scdb

 
function displayQuestionIn(){
	var x=9;
	var y=12;
var rndNo=Math.floor(Math.random()*((y-x)+1)+x);

 var inq1;
 var inq2;
 var inq3;
 var inq4;

if(rndNo==9){inq1=queBanks[questionNo][1];inq2=queBanks[questionNo][2];inq3=queBanks[questionNo][3];inq4=queBanks[questionNo][4];}
if(rndNo==10){inq2=queBanks[questionNo][1];inq3=queBanks[questionNo][2];inq4=queBanks[questionNo][3];inq1=queBanks[questionNo][4];}
if(rndNo==11){inq3=queBanks[questionNo][1];inq4=queBanks[questionNo][2];inq1=queBanks[questionNo][3];inq2=queBanks[questionNo][4];}
if(rndNo==12){inq4=queBanks[questionNo][1];inq3=queBanks[questionNo][2];inq2=queBanks[questionNo][3];inq1=queBanks[questionNo][4];}

if(accOn === true && queBanks[questionNo][0] === 'What is another name for the cardiac chamber of the crayfish stomach?' ) {	

    $('#acc_9').css('top','104px');
	$('#acc_10').css('top','173px');
	$('#acc_11').css('top','241px');
	$('#acc_12').css('top','309px');
    }
 if(accOn === true && queBanks[questionNo][0] === 'Which muscle groups in the abdomen help a crayfish propel backward?' ) {	
  
    $('#acc_9').css('top','104px');
	$('#acc_10').css('top','173px');
	$('#acc_11').css('top','241px');
	$('#acc_12').css('top','309px');
    }


$(curQue).append('<div class="SlideQuestion">'+queBanks[questionNo][0]+'</div><div class="ansMain"><div id="9" class="customRadioIn answer">'+inq1+'</div><div id="acc_9" class="accTxt"><div class="arrow-left"></div><p>1</p></div><div id="10" class="customRadioIn answer">'+inq2+'</div><div id="acc_10" class="accTxt"><div class="arrow-left"></div><p>2</p></div><div id="11" class="customRadioIn answer">'+inq3+'</div><div id="acc_11" class="accTxt"><div class="arrow-left"></div><p>3</p></div><div id="12" class="customRadioIn answer">'+inq4+'</div><div id="acc_12" class="accTxt"><div class="arrow-left"></div><p>4</p></div></div>');

if(accOn === true){$('#acc_9, #acc_10, #acc_11, #acc_12').css('visibility','visible'); }

 $('.customRadioIn').click(function(){
	 
  if(queLocks==false){queLocks=true;
  $('#9').css("opacity",0.5);
	 $('#10').css("opacity",0.5);
	 $('#11').css("opacity",0.5);
	$('#12').css("opacity",0.5);	 
		
	$('#'+this.id).css("opacity",1);	
	$('#'+rndNo).css("opacity",1);	
  $('.customRadioIn').addClass('no-hover');
  //correct answer
  if(this.id==rndNo){
	    $(this).css( "background", "rgba(0, 160, 81, 0.23)","color", "#000");
   //$(stage).append('<div class="feedback1">CORRECT</div>');
   inscore++;
   }
  //wrong answer	
  if(this.id!=rndNo){
  // $(stage).append('<div class="feedback2">WRONG</div>');
  	 $(this).css( "background", "rgba(231, 28, 36, 0.25)","color", "#000");
    $('#' + rndNo).css( "background", "rgba(0, 160, 81, 0.23)","color", "#000");
	
  }
  
  $(curQue).append('<a class="checkBtn"  href="#">Proceed </a><div id="acc_inquiz" class="accTxt"><div class="arrow-right"></div><p>P</p></div>');
   if(accOn === true){$('#acc_inquiz').css('visibility','visible'); }
  if(accOn === true && queBanks[questionNo][0] == 'What is another name for the cardiac chamber of the crayfish stomach?' ) {	
   	$('#acc_inquiz').css('top' ,'396px');
    
    }
	 if(accOn === true && queBanks[questionNo][0] == 'Which muscle groups in the abdomen help a crayfish propel backward?' ) {	
   	$('#acc_inquiz').css('top' ,'396px');
	 }
   $(".checkBtn").click(function() { 
   changeQuestionIn() ;
  });
  
 }})
}//display question


	function changeQuestionIn(){
		
		questionNo++;
	
	if(curQue=="#curQue"){
		nexQue="#curQue";
		curQue="#nexQue";
	}
		else{
			nexQue="#nexQue";
			curQue="#curQue";
			}
	
	if(questionNo<numOfQue){
		displayQuestionIn();
		}
		else{
			displayFinalSlideIn();
			}
	
	 $(nexQue).animate({},"slow", function() {$(nexQue),$(nexQue).empty();});
	 $(curQue).animate({},"slow", function() {queLocks=false;});
	}//change question
	
	
	function displayFinalSlideIn(){
		
		$(curQue).append('<div class="SlideQuestion">You have answered  '+inscore+' correct out of '+numOfQue+' questions.</div>');
		$(curQue).append("<p class='slideCon'>Now select either the Overview or External button below.</p>");
		//$(curQue).append('<br><div class="retryin">Retry</div>');
		$('.internalBtn').click(function(){inscore=0;questionNo=-1;changeQuestionIn();});	
		
			var overviewBtn = $('.overviewBtn');
			var internalBtn = $('.internalBtn');
			var externalBtn = $('.externalBtn');
	 		overviewBtn.prop("disabled",false);
			$(".overviewBtn").removeClass("ovdeactive");
			externalBtn.prop("disabled",false);
			$(".externalBtn").removeClass("exdeactive");
			overviewBtn.css('pointer-events','');
			externalBtn.css('pointer-events','');
			
			internalBtn.prop("disabled",true);
			$(".internalBtn").addClass("indeactive");
			
			$('#acc_overview01').show();
			$('#acc_external').show();
			$('#acc_internal').hide();
	 	
	}//display final slide

	return false;
	}//doc ready